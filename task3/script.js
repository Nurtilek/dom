const form = document.getElementById('bg');
const btn = document.querySelector('button');
const bg = document.querySelector('div');

btn.addEventListener('click', () => {
    bg.style.backgroundImage = `url("${form.value}")`;
})

bg.style.width = '400px';
bg.style.height = '400px';
bg.style.backgroundPosition  = 'center';
bg.style.backgroundSize  = 'cover';
bg.style.backgroundColor = 'black';
bg.style.marginTop = '15px';