//Первое задание

const name = document.getElementById('name');
const surname = document.getElementById('surname');
const lastname = document.getElementById('lastname');
const btn = document.getElementById('btn');
const p = document.querySelector('p');
const input = document.querySelector('input');
p.style.textTransform = 'uppercase';

btn.disabled = true;

btn.addEventListener('click', ()=>{
  p.textContent = `${surname.value[0]}.${name.value[0]}.${lastname.value}`
})

lastname.addEventListener('input', () => {
  btn.disabled = !(name.value && surname.value && lastname.value)
})

name.addEventListener('input', () => {
  btn.disabled = !(name.value && surname.value && lastname.value)
})

surname.addEventListener('input', () => {
  btn.disabled = !(name.value && surname.value && lastname.value)
})


